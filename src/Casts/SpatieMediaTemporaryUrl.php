<?php

namespace KDA\Laravel\Attributes\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Support\Facades\Storage;

class SpatieMediaTemporaryUrl implements CastsAttributes
{

    public function __construct(
        protected $collection = null,
        protected $conversion = null
    ) {
    }

    /**
     * Cast the given value.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return mixed
     */
    public function get($model, string $key, $value, array $attributes)
    {
        $media =  $model->getFirstMedia($collection);

        if (!$media) {
            return null;
        }
        $imagePath = $media?->getPathRelativeToRoot($conversion);
        return   $imagePath ? Storage::disk($media->disk)->temporaryUrl($imagePath, now()->addMinutes(2)) : null;
    }

    /**
     * Prepare the given value for storage.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return mixed
     */
    public function set($model, string $key, $value, array $attributes)
    {
        return $value;
    }
}
