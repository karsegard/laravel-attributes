<?php

namespace KDA\Laravel\Attributes\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class SpatieMediaUrl implements CastsAttributes
{


    public function __construct(
        protected $collection = null,
        protected $conversion = null
    ) {
    }
    /**
     * Cast the given value.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return mixed
     */
    public function get($model, string $key, $value, array $attributes)
    {
        $media =  $model->getFirstMedia($this->collection);

        return $media ? $media->getUrl($this->conversion) : NULL;
    }

    /**
     * Prepare the given value for storage.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return mixed
     */
    public function set($model, string $key, $value, array $attributes)
    {
        return $value;
    }
}
